from django.urls import path
from . import views



urlpatterns = [
    path('loycard/<str:store>/<str:pos_id>/<str:date>', views.loycard_request, name='loycard_request'),
    path('loycard', views.loycard, name='loycard'),
    path('barcode/<str:store>/<str:pos_id>/<str:date>', views.barcode_request, name='barcode_request'),
    path('barcode', views.barcode, name='barcode'),
    path('spasibo/<str:store>/<str:pos_id>/<str:date>', views.spasibo_request, name='spasibo_request'),
    path('spasibo', views.spasibo, name='spasibo'),
    path('stoloto/<str:store>/<str:pos_id>/<str:date>', views.stoloto_request, name='stoloto_request'),
    path('stoloto', views.stoloto, name='stoloto'),
    path('soccard/<str:store>/<str:pos_id>/<str:date>', views.soccard_request, name='soccard_request'),
    path('soccard', views.soccard, name='soccard'),
    path('stiker/<str:store>/<str:pos_id>/<str:date>', views.stiker_request, name='stiker_request'),
    path('stiker', views.stiker, name='stiker'),
    path('tcx/<str:store>/<str:pos_id>/<str:date>', views.tcx_request, name='tcx_request'),
    path('tcx', views.tcx, name='tcx'),
    path('tc5/<str:store>/<str:pos_id>/<str:date>/<str:page>', views.tc5_request, name='tc5_request'),

]