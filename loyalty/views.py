from django.shortcuts import render
from . import loycard_parser
from . import barcode_parser
from . import spasibo_parser
from . import stoloto_parser
from . import soccard_parser
from . import stiker_parser
from . import tcx_parser
from . import tc5_parser
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
import json
import os
import logging
from logging.handlers import RotatingFileHandler



log_level = logging.INFO
LOG_PATH = os.getcwd() + "/logs/loyalty.log"
logFormatter = logging.Formatter(fmt='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)
logger.info('requests log')
logger.setLevel(log_level)
logfile = LOG_PATH
fileHandler = logging.FileHandler(logfile)
fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(log_level)
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rotateHandler = RotatingFileHandler(logfile, maxBytes=1024 * 1024 * 50, backupCount=1)
rotateHandler.setFormatter(logFormatter)
logger.addHandler(rotateHandler)
# logger.addHandler(consoleHandler)



# Create your views here.

def logging(function):
    def wrapper(request,store, pos_id, date, page=None):
        logger.info("***********************REQUEST***********************")
        logger.info('User:{}'.format(request.user.username))
        logger.info('Action:{}'.format(function.__name__))
        logger.info('Sap:{}'.format(store))
        logger.info('Pos:{}'.format(pos_id))
        logger.info('Date:{}'.format(date))
        logger.info("*****************************************************")
        if page:
            f=function(request, store, pos_id, date, page)
        else:
            f=function(request, store, pos_id, date)
        return f
    return wrapper

@login_required
def loycard(request):
    return render(request, 'loyalty/loycard.html')

@login_required
@csrf_exempt
@logging
def loycard_request(request, store, pos_id, date):
    transactions = loycard_parser.parser(store, pos_id, date)
    return JsonResponse(transactions, safe=False)

@login_required
def barcode(request):
    return render(request, 'loyalty/barcode.html')
	
@login_required
@csrf_exempt
@logging
def barcode_request(request, store, pos_id, date):
    transactions = barcode_parser.parser(store, pos_id, date)
    return JsonResponse(transactions, safe=False)

@login_required
def spasibo(request):
    return render(request, 'loyalty/spasibo.html')
	
@login_required
@csrf_exempt
@logging
def spasibo_request(request, store, pos_id, date):
    transactions = spasibo_parser.parser(store, pos_id, date)
    return JsonResponse(transactions, safe=False)
@login_required
def stoloto(request):
    return render(request, 'loyalty/stoloto.html')
	
@login_required
@csrf_exempt
@logging
def stoloto_request(request, store, pos_id, date):
    transactions = stoloto_parser.parser(store, pos_id, date)
    return JsonResponse(transactions, safe=False)
	
@login_required
def soccard(request):
    return render(request, 'loyalty/soccard.html')
	
@login_required
@csrf_exempt
@logging
def soccard_request(request, store, pos_id, date):
    transactions = soccard_parser.parser(store, pos_id, date)
    return JsonResponse(transactions, safe=False)
	
@login_required
def stiker(request):
    return render(request, 'loyalty/stiker.html')
	
@login_required
@csrf_exempt
@logging
def stiker_request(request, store, pos_id, date):
    transactions = stiker_parser.parser(store, pos_id, date)
    return JsonResponse(transactions, safe=False)
	
@login_required
def tcx(request):
    return render(request, 'loyalty/tcx.html')
	
@login_required
@csrf_exempt
@logging
def tcx_request(request, store, pos_id, date):
    transactions = tcx_parser.parser(store, pos_id, date)
    return JsonResponse(transactions, safe=False)



@login_required
@csrf_exempt
@logging
def tc5_request(request, store, pos_id, date, page):
    transactions = tc5_parser.parser(store, pos_id, date, page)
    return JsonResponse(transactions, safe=False)



