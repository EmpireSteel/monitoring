var dict = {'type': 'Тип', 'partnerId': 'ID партнера (идентификатор ТС)', 'locationCode': 'SAP магазина','terminalCode': 'Номер кассы','transactionDate': 'Время транзакции',
'receiptId': 'Номер чека','cardno': 'Номер карты','amount': 'Сумма чека','ip_cash_desk': 'IP кассы', 'processingDate':'Время проведения',
'points':'Баллы', 'balance':'Баланс по карте', 'text':'Сообщение', 'errorCode':'Код ошибки от Comarch', 'discount':'Скидка', 'totp': 'totp'}

$(document).ready(function () {
    $("#loyalty_card_parser").click(
        function () {
            var page = $(location).attr('href').split(/[/ ]+/).pop();
            var host = $("#store_input").val();
            host = host.replace(/\s/g, '');
            var pos_id = $('#pos_input').val();
            var trans_date = $('#trans_date').val();
            if (host != '' && pos_id != '' && trans_date != '') {
                $('#loyalty_card_parser').attr("disabled", true);
                $('#loyalty_card_parser').append('<span id="spiner" class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>');
                $.ajax({
                    url: "/loyalty/tc5/" + page + "/" + host + "/" + pos_id + "/" + trans_date,
                    success: function (result) {
                        console.log(result);
                        
                        if ('error' in result) {
                            alert(result.error)
                        } else {
                            var table = '<table id="trans_table" class="table table-striped table-bordered" cellspacing="0" width="100%"><thead><tr>';
                            table += '<td>Тип носителя</td>';
                            table += '<td>Тип операции</td>';
                            table += '<td>Время</td>';
                            table += '<td>№ карты</td>';
                            table += '<td>№ чека</td>';
                            table += '<td>Ошибка</td>';
                            table += '<td>Детали</td>';
                            table += '</tr>';
                            table += '</thead>';
                            table += '<tbody>';
                            $.each(result.data, function (index, value) {
                                table += '<td>' + value.request.type_name + '</td>';
                                table += '<td>' + value.request.name + '</td>';
                                table += '<td>' + value.request.transactionDate + '</td>';
                                table += '<td>' + value.request.cardno + '</td>';
                                table += '<td>' + value.request.receiptId + '</td>';
                                table += '<td>' + value.response.errorCode + '</td>';
                                table += '<td style="max-width:800px;width:800px">';
                                table += '<div class="card" >';
                                table += '<div class="card-header bg-info  text-white" id="request_header' + index + '" tabindex="0" data-toggle="collapse" data-target="#request_body' + index + '" aria-expanded="true" aria-controls="#request_body' + index + '">';
                                table += '<h6 class=" mb-0">Запрос</h6></div>';
                                table += '<div id="request_body' + index + '" class="collapse" aria-labelledby="request_header' + index + '">';
                                table += '<div class="card-body-block bg-light">';
                                table += '<table class="table table-sm text-center mb-0">';
                                // Request
                                table += '<tbody>';
                                $.each(value.request, function(i, v){
                                    if (i in dict){
                                    table +='<tr><td>'+dict[i] + '</td><td>' + v + '</td></tr>';
                                    }
                                    // else {
                                    //     table +='<tr><td>'+ i + '</td><td>' + v + '</td></tr>';
                                    // }
                                })
                                if ('products' in value.request) {
                                    if (value.request.products.item.length > 0){
                                        table += '<tr><td>Товары</td>';
                                        table += '<td><table class="table table-sm text-center">';
                                        table += '<thead><tr>';
                                        table += '<th>Группа</th><th>PLU</th><th>Кол-во</th><th>Цена</th></tr></thead><tbody>';
                                        $.each(value.request.products.item, function (product_index, product_value) {
                                            table += '<tr>';
                                            $.each(product_value, function (product_i, product_v) {
                                            table += '<td>' + product_v + '</td>';                    
                                            })
                                            table += '</tr>';
                                        })
                                        table += '</tbody>';
                                        table += '</table></td></tr>';
                                    }
                                } 
                                else {
                                    table += '<td></td></tr>';
                                }
                                table += '</tbody>';
                                table += '</table>';
                                table += '</div>';
                                table += '</div>';
                                table += '</div>';
                                table += '<div class="card">';
                                table += '<div class="card-header bg-info  text-white" id="response_header' + index + '" tabindex="0" data-toggle="collapse" data-target="#response_body' + index + '" aria-expanded="true" aria-controls="#response_body' + index + '">';
                                table += '<h6 class=" mb-0">Ответ</h6></div>';
                                table += '<div id="response_body' + index + '" class="collapse" aria-labelledby="response_header' + index + '">';
                                table += '<div class="card-body-block bg-light">';
                                table += '<table class="table table-sm ">';
                                table += '<tbody>';
                                // Response
                                $.each(value.response, function(i, v){
                                    if (i in dict){
                                        if (i == 'text'){
                                            if (v == null){
                                                table += '<tr><td align="center">' + dict[i] + '</td><td align="center">' + v + '</td></tr>';
                                            }
                                            else {
                                                if (value.request.type  == 'I') {
                                                    table += '<tr><td align="center">'+ dict[i] +'</td><td><pre style="line-height:0.5;overflow: hidden;"class="py-2 mb-0 ">' + v + '</pre></td></tr>';
                                                }
                                                else {
                                                    table += '<tr><td align="center">'+ dict[i] +'</td><td><pre style="line-height:1;overflow: hidden;"class="py-2 mb-0 ">' + v + '</pre></td></tr>';
                                                }
                                            }
                                        }
                                        else {
                                        table +='<tr><td align="center">'+dict[i] + '</td><td align="center">' + v + '</td></tr>';
                                        }
                                    }
                                })
                                table += '</tbody>';
                                table += '</table>';
                                table += '</div>';
                                table += '</div>';
                                table += '</div>';
                                table += '</td>';
                                table += '</tr>';
                            });
                            table += '</tbody>';
                            table += '</table>';
                            $('#transactions').html(table);
                        }
                        $('#trans_table').DataTable({
                            "order": [
                                [2, "asc"]
                            ],
                            "paging": false,
                            "language": {
                                "processing": "Подождите...",
                                "search": "Поиск:",
                                "lengthMenu": "Показать _MENU_ записей",
                                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                                "infoPostFix": "",
                                "loadingRecords": "Загрузка записей...",
                                "zeroRecords": "Записи отсутствуют.",
                                "emptyTable": "В таблице отсутствуют данные",
                                "paginate": {
                                  "first": "Первая",
                                  "previous": "Предыдущая",
                                  "next": "Следующая",
                                  "last": "Последняя"
                                },
                                "aria": {
                                  "sortAscending": ": активировать для сортировки столбца по возрастанию",
                                  "sortDescending": ": активировать для сортировки столбца по убыванию"
                                }
                              }
                        })
                        $('.dataTables_length').addClass('bs-select');
                    },

                    complete: function () {
                        $('#spiner').remove();
                        $('#loyalty_card_parser').attr("disabled", false);
                    },
                    error: function (result) {
                        alert('Неизвестная ошибка. Опишите запрос на andrey.gavrilov@x5.ru.');
                    }
                });
            }
        });
});