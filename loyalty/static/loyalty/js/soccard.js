$(document).ready(function ()
    {
    $("#loyalty_card_parser").click(
        function () {
            var host = $("#store_input").val();
            host = host.replace(/\s/g, '');
            var pos_id = $('#pos_input').val();
            var trans_date = $('#trans_date').val();
            if (host != '' && pos_id != '' && trans_date != ''){
                $('#loyalty_card_parser').attr("disabled", true);
                $('#loyalty_card_parser').append('<span id="spiner" class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>');
                $.ajax({
                        url: "/loyalty/soccard/" + host + "/" + pos_id + "/" + trans_date,
                        success: function (result) {
                            if ('error' in result) {
                                alert(result.error)
                            } 
                            else if (result.data.length == 0)
                            {
                                alert("Нет данных")
                            }
                            else {
                                if (result.data.length > 0) {
                                    var table = '<table id="trans_table" class="table table-striped table-bordered" cellspacing="0" width="100%"><thead><tr>';
                                    table +='<td>Тип операции</td>';
                                    table +='<td>Время</td>';
                                    table +='<td>№ карты</td>';
                                    table +='<td>№ чека</td>';
                                    table +='<td>Ошибка</td>';
                                    table +='<td>Детали</td>';
                                    table +='</tr>';
                                    table +='</thead>';
                                    table +='<tbody>';
                                    $.each(result.data,function(index,value){
                                        if (value.success_response == 1) {
                                            if (value.response_errorCode == '0') {
                                                table += '<tr class="table-success">';
                                            }
                                            else {
                                                table += '<tr class="table-warning">';
                                            }
                                            
                                        }
                                        else
                                        {
                                            table += '<tr class="table-danger">';
                                        }

                                        table += '<td>' + value.transactions_name + '</td>';
                                        table += '<td>' + value.transactionDate + '</td>';
                                        table += '<td>' + value.cardno + '</td>';
                                        table += '<td>' + value.receiptId + '</td>';
                                        table += '<td>' + value.response_errorCode + '</td>';
                                        table += '<td style="max-width:800px;width:800px">';
                                        table += '<div class="card" >';
                                        table += '<div class="card-header bg-info  text-white" id="request_header' + index + '" tabindex="0" data-toggle="collapse" data-target="#request_body' + index + '" aria-expanded="true" aria-controls="#request_body' + index + '">';
                                        table += '<h6 class=" mb-0">Запрос</h6></div>';
                                        table += '<div id="request_body' + index + '" class="collapse" aria-labelledby="request_header' + index + '">';
                                        table += '<div class="card-body-block bg-light">';
                                        table += '<table class="table table-sm text-center mb-0">';
                                        table += '<tbody>';
                                        table += '<tr><td>Тип</td><td>' + value.transaction_type + '</td></tr>';
                                        table += '<tr><td>ID партнера (идентификатор ТС)</td><td>' + value.partnerId + '</td></tr>';
                                        table += '<tr><td>SAP магазина</td><td>' + value.locationCode + '</td></tr>';
                                        table += '<tr><td>Номер кассы</td><td>' + value.terminalCode + '</td></tr>';
                                        table += '<tr><td>Время транзакции</td><td>' + value.transactionDate + '</td></tr>';
                                        table += '<tr><td>Номер чека</td><td>' + value.receiptId + '</td></tr>';
                                        table += '<tr><td>Номер карты</td><td>' + value.cardno + '</td></tr>';
                                        table += '<tr><td>Сумма чека</td><td>' + value.amount + '</td></tr>';
                                        if (value.transaction_type == 'R'){
                                        table += '<tr><td>Запрошенно баллов к списанию</td><td>' + value.points_pbp + '</td></tr>';
                                        };
                                        table += '<tr><td colspan="2">Товары</td></tr>';
                                        table += '</tbody>';
                                        table += '</table>';
                                        if (value.products.length > 0) {
                                            table += '<table class="table table-sm text-center">';
                                            table += '<thead><tr>';
                                            table += '<td>PLU</td><td>Кол-во</td><td>Цена</td>';
                                            table += '</tr></thead>';
                                            table += '<tbody>';
                                            $.each(value.products,function(product_index,product_value)
                                            {
                                                table += '<tr>';
                                                table += '<td>' + product_value.code + '</td>';
                                                table += '<td>' + product_value.quantity + '</td>';
                                                table += '<td>' + product_value.amount + '</td>';
                                                table += '</tr>';
                                            })
                                            table += '</tbody>';
                                            table += '</table>';
                                        };
                                        table += '</div>';
                                        table += '</div>';
                                        table += '</div>';
                                        table += '<div class="card">';
                                        table += '<div class="card-header bg-info  text-white" id="response_header' + index + '" tabindex="0" data-toggle="collapse" data-target="#response_body' + index + '" aria-expanded="true" aria-controls="#response_body' + index + '">';
                                        table += '<h6 class=" mb-0">Ответ</h6></div>';
                                        table += '<div id="response_body' + index + '" class="collapse" aria-labelledby="response_header' + index + '">';
                                        table += '<div class="card-body-block bg-light">';
                                    table += '<table class="table table-sm text-center">';
                                        table += '<tbody>';
                                        table += '<tr><td>Номер чека</td><td>' + value.response_receiptId + '</td></tr>';
                                        table += '<tr><td>Время транзакции</td><td>' + value.response_processingDate + '</td></tr>';
                                        if (value.transaction_type == 'R') {
                                        table += '<tr><td>Списано баллов</td><td>' + value.response_points + '</td></tr>';
                                        }
                                        else{
                                        table += '<tr><td>Доступно к списанию баллов</td><td>' + value.response_points + '</td></tr>';
                                        };

                                        table += '<tr><td>Баланс</td><td>' + value.response_balance + '</td></tr>';
                                        table += '<tr><td>Текст</td><td>' + value.response_text + '</td></tr>';
                                        table += '<tr><td>Код ошибки от Comarch</td><td>' + value.response_errorCode + '</td></tr>';
                                        table += '</tbody>';
                                        table += '</table>';
                                        table += '</td>';
                                        table += '</tr>';
                                    });
                                    table +='</tbody>';
                                    table +='</table>';
                                    $('#transactions').html(table);
                                }
                                $('#trans_table').DataTable(
                                                        {
                                        "order": [[ 1, "asc" ]],
                                        "paging":   false,
                                    }




                                )
                                $('.dataTables_length').addClass('bs-select');}
                        },
                        complete: function(){
                            $('#spiner').remove();
                            $('#loyalty_card_parser').attr("disabled", false);
                        },
                        error: function (result) {
                            alert('Неизвестная ошибка. Опишите запрос на andrey.gavrilov@x5.ru.');
                        }
                });
            }
            else {
                alert('Не все поля заполнены')
            }
        });
    },
    $(document).on('keypress',function(e) {
        if(e.which == 13) {
            $("#loyalty_card_parser").click();
        }
    })
);

