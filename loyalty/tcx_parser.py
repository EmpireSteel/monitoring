import os
import paramiko
import xml.etree.ElementTree as ET
from pyzabbix import ZabbixAPI
from datetime import datetime
import gzip
import xmltodict
import json
from scp import SCPClient
from . import account


def get_log(ip, log):
    client = paramiko.SSHClient()
    print('paramiko.SSHClient()')
    # client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip, username=account.tcx_login, password=account.tcx_password)
    print('connect')
    scp = SCPClient(client.get_transport())
    print('scp')
    localpath = os.getcwd() + '/loyalty/downloads/{}_{}'.format(ip.replace('.', ''), log)
    print(localpath)
    with open(localpath, 'w') as f:
        pass
    remotepath = 'C:/EXEC/log/{}'.format(log)
    print(remotepath)
    scp.get(remotepath, localpath)
    print('get')
    scp.close()
    client.close()
    with open(localpath, encoding='cp1251') as f:
        lines = f.readlines()
    os.remove(localpath)
    return lines


def get_transactions(lines, transactions, start):
    transaction = {'request': {}, 'response': {}}
    transaction, stop = get_transaction(lines, start)
    transactions.append(transaction)
    return transactions, stop


def get_transaction(lines, start):
    request_txt = '<request>'
    request_txt += lines[start].split('Сообщение=')[-1].strip()
    for i, line in enumerate(lines[start+1:]):
        if not '</pos_version>' in line:
            request_txt += line.strip().replace('\t', '')
        else: 
            request_txt += line.strip().replace('\t', '')
            request_txt += '</request>'
            stop = i + start + 2
            break
    request = json.loads(json.dumps(xmltodict.parse(request_txt)))
    date = request['request']['transactionDate']
    request['request']['transactionDate'] = date[6:8] + ':' + date[8:10] + ':' + date[10:12] + ' ' + date[4:6] + '-' + date[2:4] + '-' + date[0:2]           
    for index, line in enumerate(lines[stop:]):
        if '|Ответ=' in line:
            response_txt = '<response>'
            response_txt += line.split('|Ответ=')[-1]
            c = 0
            for l in lines[stop + 1 + index:]:
                c += 1
                try:
                    int(l[:4])
                    break
                except:
                    response_txt += l
            response_txt = response_txt[:-3] + '</response>'
            stop = stop + index
            break
    text = response_txt.split('<text>')[-1].split('</text>')[0]
    if text:
        response_txt = ''.join(response_txt.split(text))
    response = json.loads(json.dumps(xmltodict.parse(response_txt)))
    date = response['response']['processingDate']
    response['response']['processingDate'] = date[6:8] + ':' + date[8:10] + ':' + date[10:12] + ' ' + date[4:6] + '-' + date[2:4] + '-' + date[0:2] 
    if text:
        response['response']['text'] = text.replace(';', '<br>')
    transaction = {'request': request['request'], 'response': response['response']}
    transaction['request']['type_name'], transaction['request']['name'] = get_type_name(lines, transaction, start, stop)      

    return transaction, stop
    

def get_type_name(lines, transaction, start, stop):
    if transaction['request']['type'] == 'B':
        if 'VIP-Карта=Скидка|Сообщение=||' in lines[stop+1]:
            return 'VIP-Карта', 'Запрос баланса'
        if 'discount' in transaction['response']:
            return 'Купон', 'Проверка купона'
        if 'Соц.баланс|Старт||' in lines[start-2]:
            return 'Соц. карта', 'Запрос баланса'
        if 'Баланс|Старт||' in lines[start-1] or 'Баланс|Старт||' in lines[start-2]:
            return 'Карта лояльности', 'Запрос баланса'
    elif transaction['request']['type'] == 'I':
        return 'Карта лояльности', 'Начисление баллов'
    elif transaction['request']['type'] == 'U':
        return 'Карта лояльности', 'Отмена'
    elif transaction['request']['type'] == 'F':
        return 'Карта лояльности', 'Возврат'
    elif transaction['request']['type'] == 'R':
        if 'Списание c соц.карты|Старт||' in lines[start-3]:
            return 'Соц. карта', 'Списание баллов'
        if 'категория покупателя|99||' in lines[start-5]:
            return 'VIP-Карта', 'Скидка'
        if transaction['response']['text']:
            if 'купон' in transaction['response']['text'].lower():
                for i in ['.', ',']:
                    transaction['response']['text'] = transaction['response']['text'].replace(i, i + '<br>')
                return 'Купон', 'Гашение купона'
        if 'Списание|Старт||' in lines[start-3]:
            return 'Карта лояльности', 'Списание баллов'
    elif transaction['request']['type'] == 'G':
        transaction['response']['text'] = transaction['response']['text'].replace('&', '<br>').replace('*', '*<br>')
        return 'Столото', 'Продажа билета'
    return ' ', ' '


def log_parser(lines):
    transactions = []
    skip = 0
    for index, line in enumerate(lines):
        if index < skip:
            continue
        if 'Сообщение=<type>' in line:
            transactions, skip = get_transactions(lines, transactions, index)

    return transactions
        



def get_ip(host, pos_id):
    z = ZabbixAPI('http://zabbix-head.x5.ru/')
    z.login("store_card_api", "12345")
    result = z.do_request('host.get', {
        'search': {'host': host},
        'selectInterfaces': ['ip', 'dns'],
        'output': ['name','interfaces']
    })['result'][0]
    z.session.close()
    for i in result['interfaces']:
        if i['dns'].startswith('POS{}'.format(pos_id)):
            return i['ip']
    return ''


def get_log_name(date):
    now = str(datetime.now().date())
    date = date.replace('-', '')
    log = '_' + date + '.log'
    return log


def parser(host, pos_id, date):
    # try:
    print('get_log_name')
    log_name = get_log_name(date)
    print('get_ip')
    ip = get_ip(host, pos_id)
    print('get_log')
    lines = get_log(ip, log_name)
    if not lines:
        return {'data': []}
    print('log_parser')
    transactions = log_parser(lines)
    # except FileNotFoundError:
    #     return {'error': 'Лог не найден'}
    # except Exception:
    #     return {'error': 'Неизвестная ошибка. Опишите запрос на andrey.gavrilov@x5.ru.'}
    return {'data': transactions}
