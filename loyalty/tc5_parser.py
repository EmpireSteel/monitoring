import os
import paramiko
import xml.etree.ElementTree as ET
from pyzabbix import ZabbixAPI
from datetime import datetime
import gzip
import xmltodict
import json
from scp import SCPClient
from . import account


def get_log(ip, logs):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip, username=account.tc5_login, password=account.tc5_password)
    sftp = ssh.open_sftp()
    localpaths = []
    for log in logs:
        localpath = os.getcwd() + '/loyalty/downloads/{}_{}'.format(str(datetime.now().timestamp()).split('.')[0], log)
        with open(localpath, 'w') as f:
            pass
        remotepath = '/usr/local/gkretail/pos/log/{}'.format(log)
        try:
            sftp.get(remotepath, localpath)
            localpaths.append(localpath)
        except FileNotFoundError:
            pass
    sftp.close()
    ssh.close()
    lines = []
    for localpath in localpaths:
        if '.gz' in localpath:
            f = gzip.GzipFile(localpath, "rb")
            line = f.readlines()
            f.close()
            for i, l in enumerate(line):
                line[i] = line[i].decode('utf-8')
        else:
            with open(localpath, encoding='UTF-8') as f:
                line = f.readlines()
        lines.append(line)
        os.remove(localpath)
    return lines


def log_parser_choose(page, lines):
    transactions = []
    if page == 'barcode':
        transactions = barcode_parser(lines)
    elif page == 'loycard':
        transactions = loycard_parser(lines)
    # elif page == 'sticker':
    #     transactions = sticker_parser(lines)
    # elif page == 'soccard':
    #     transactions = soccard_parser(lines)
    # elif page == 'spasibo':
    #     transactions = spasibo_parser(lines)
    # elif page == 'stolotto':
    #     transactions = stolotto_parser(lines)
    return transactions
        

def get_ip(host, pos_id):
    if int(pos_id) < 10:
        pos_id = '0' + pos_id
    host = 'POS{}-BO-{}'.format(pos_id, host)
    z = ZabbixAPI('http://msk-dpro-app351/')
    z.login("store_card_api", "12345")
    ip = z.do_request('host.get', {
        'search': {'host': host},
        'selectInterfaces': ['ip'],
        'output': ['interfaces']
    })['result'][0]['interfaces'][0]['ip']
    z.session.close()
    return ip


def get_logs_name(page, date):
    print(page)
    now = str(datetime.now().date())
    if date == now:
        if page == 'barcode':
            logs = ['barcode.log', 'loycard.log']
        elif page == 'loycard' or page == 'spasibo' or page == 'sticker':
            logs = ['loycard.log']
        elif page == 'soccard':
            logs = ['soccard.log']
        elif page == 'stolotto':
            logs = ['stolotto.log']    
    else:
        date = date.replace('-', '')
        if page == 'barcode':
            logs = ['barcode.log-' + date + '0000.gz', 'loycard.log-' + date + '0000.gz']
        elif page == 'loycard' or page == 'spasibo' or page == 'stiker':
            logs = ['loycard.log-' + date + '0000.gz']
        elif page == 'soccard':
            logs = ['soccard.log-' + date + '0000.gz']
        elif page == 'stolotto':
            logs = ['stolotto.log-'  + date + '0000.gz']    
    return logs


def parser(host, pos_id, date, page):
    # try:
    print(pos_id)
    log_name = get_logs_name(page, date)
    ip = get_ip(host, pos_id)
    if not ip:
        return {'error': 'Касса не найдена'}
    lines = get_log(ip, log_name)
    # if not lines:
    #     return {'data': []}
    transactions = log_parser_choose(page, lines)
    # except FileNotFoundError:
    #     return {'error': 'Лог не найден'}
    # except Exception:
    #     return {'error': 'Неизвестная ошибка. Опишите запрос на andrey.gavrilov@x5.ru.'}
    return {'data': transactions}


def barcode_parser(logs):
    transactions = []
    transaction = {}
    state = 'request'
    for log in logs:
        for i, line in enumerate(log):
            if state == 'request':
                if '[PlBarcodeManagement] REQUEST:' in line:
                    request_txt = line.split('REQUEST: ')[-1]
                    request = json.loads(json.dumps(xmltodict.parse(request_txt)))
                    request = request['ns1:ProcessTransaction']['RequestData']
                    date = request['transactionDate']
                    request['transactionDate'] = date[6:8] + ':' + date[8:10] + ':' + date[10:12] + ' ' + date[4:6] + '-' + date[2:4] + '-' + date[0:2]           
                    transaction['request'] = request
                    state = 'response'
                elif '[PlLoyaltyCardManagement] REQUEST:' in line:
                    request_txt = line.split('REQUEST: ')[-1]
                    request = json.loads(json.dumps(xmltodict.parse(request_txt)))
                    request = request['ns1:ProcessTransaction']['RequestData']
                    print(request)
                    date = request['transactionDate']
                    request['transactionDate'] = date[6:8] + ':' + date[8:10] + ':' + date[10:12] + ' ' + date[4:6] + '-' + date[2:4] + '-' + date[0:2]           
                    transaction['request'] = request
                    if transaction['request']['type'] == 'I':
                        state = 'response'
                    else:
                        continue
            elif state == 'response': 
                if '[PlBarcodeManagement] RESPONSE:' in line:
                    response_txt = line.split('RESPONSE: ')[-1]
                    for l in lines[i + 1:]:
                            try:
                                int(l[:4])
                                break
                            except:
                                response_txt += l
                    response = json.loads(json.dumps(xmltodict.parse(response_txt)))
                    response = response['ns1:ProcessTransactionResponse']['ResponseData'] 
                    response['success'] = 1
                    transaction['response'] = response
                    if 'products' in transaction['response']:
                        for i in transaction['response']['item']:
                            if i['groupCode'] == '909':
                                transaction['request']['ticket'] = i['code']
                    state = 'type' 
                elif '[PlLoyaltyCardManagement] RESPONSE:' in line:
                    response_txt = line.split('RESPONSE: ')[-1]
                    for l in log[i + 1:]:
                            try:
                                int(l[:4])
                                break
                            except:
                                response_txt += l
                    response = json.loads(json.dumps(xmltodict.parse(response_txt)))
                    response = response['ns1:ProcessTransactionResponse']['ResponseData'] 
                    response['success'] = 1
                    transaction['response'] = response
                    if 'products' in transaction['response']:
                        for i in transaction['response']['item']:
                            if i['groupCode'] == '909':
                                transaction['request']['ticket'] = i['code']
                    state = 'type' 
                elif 'TRANSACTION ERROR:' in line:
                    response = {}
                    response['success'] = 0
                    response['errorCode'] = line.split('TRANSACTION ERROR: ')[-1]
                    transaction['response'] = response
                    state = 'type' 
            elif state == 'type': 
                if transaction['request']['type'] == 'B':
                    transaction['request']['name'] = 'Запрос баланса'
                elif transaction['request']['type'] == 'I' :
                    transaction['request']['name'] = 'Выдача купона'
                elif transaction['request']['type'] == 'R':
                    transaction['request']['name'] = 'Гашение купона'
                elif transaction['request']['type'] == 'U':
                    transaction['request']['name'] = 'Отмена'
                transactions.append(transaction)
                state = 'request'
    return transactions


def loycard_parser(logs):
    transactions = []
    transaction = {}
    state = 'request'
    for log in logs:
        for i, line in enumerate(log):
            if state == 'request':
                if '[PlLoyaltyCardManagement] REQUEST:' in line:
                    request_txt = line.split('REQUEST: ')[-1]
                    request = json.loads(json.dumps(xmltodict.parse(request_txt)))
                    request = request['ns1:ProcessTransaction']['RequestData']
                    print(request)
                    date = request['transactionDate']
                    request['transactionDate'] = date[6:8] + ':' + date[8:10] + ':' + date[10:12] + ' ' + date[4:6] + '-' + date[2:4] + '-' + date[0:2]           
                    transaction['request'] = request
                    state = 'response'
            elif state == 'response':
                if '[PlLoyaltyCardManagement] RESPONSE:' in line:
                    response_txt = line.split('RESPONSE: ')[-1]
                    for l in log[i + 1:]:
                            try:
                                int(l[:4])
                                break
                            except:
                                response_txt += l
                    response = json.loads(json.dumps(xmltodict.parse(response_txt)))
                    response = response['ns1:ProcessTransactionResponse']['ResponseData'] 
                    response['success'] = 1
                    transaction['response'] = response
                    state = 'type' 
                elif 'TRANSACTION ERROR:' in line:
                    response = {}
                    response['success'] = 0
                    response['errorCode'] = line.split('TRANSACTION ERROR: ')[-1]
                    transaction['response'] = response
                    state = 'type'
            elif state == 'type': 
                if transaction['request']['type'] == 'B':
                    transaction['request']['name'] = 'Запрос баланса'
                elif transaction['request']['type'] == 'I' :
                    transaction['request']['name'] = 'Начисление'
                elif transaction['request']['type'] == 'R':
                    transaction['request']['name'] = 'Списание'
                elif transaction['request']['type'] == 'U':
                    transaction['request']['name'] = 'Отмена'
                transactions.append(transaction)
                state = 'request' 
    return transactions
