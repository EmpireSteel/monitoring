import os
import paramiko
import xml.etree.ElementTree as ET
from pyzabbix import ZabbixAPI
from datetime import datetime
import gzip
from . import account


def get_log(ip, log):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip, username=account.tc5_login, password=account.tc5_password)
    sftp = ssh.open_sftp()
    localpath = os.getcwd() + '/loyalty/logs/{}'.format(log)
    with open(localpath, 'w') as f:
        pass
    remotepath = '/usr/local/gkretail/pos/log/{}'.format(log)
    sftp.get(remotepath, localpath)
    sftp.close()
    ssh.close()
    return localpath


def parse_log(path):
    if '.gz' in path:
        f = gzip.GzipFile(path, "rb")
        lines = f.readlines()
        f.close()
        for i, line in enumerate(lines):
            lines[i] = lines[i].decode('utf-8')
    else:
        with open(path, encoding='UTF-8') as f:
            lines = f.readlines()
    transactions = []
    state = 'request'
    for i, line in enumerate(lines):
        if state == 'request':
            if '[PlSpasiboCardManagement] REQUEST:' in line:
                transaction = {}
                if '<type>B</type>' in line:
                    transaction['transactions_name'] = 'Запрос баланса'
                elif '<type>R</type>' in line:
                    transaction['transactions_name'] = 'Списание'
                elif '<type>U</type>' in line:
                    transaction['transactions_name'] = 'Отмена'
                else:
                    continue
                xml = line.split('REQUEST: ')[-1]
                root = ET.fromstring(xml)
                for data in root.iter('RequestData'):
                    transaction['transaction_type'] = data.find('type').text
                    transaction['partnerId'] = data.find('partnerId').text
                    transaction['locationCode'] = data.find('locationCode').text
                    transaction['terminalCode'] = data.find('terminalCode').text
                    date = data.find('transactionDate').text
                    transaction['transactionDate'] = date[6:8] + ':' + date[8:10] + ':' + date[10:12] + ' ' + \
                                                     date[4:6] + '-' + date[2:4] + '-' + date[0:2]
                    transaction['receiptId'] = data.find('receiptId').text
                    transaction['online'] = data.find('online').text
                    transaction['cardno'] = data.find('cardno').text
                    if transaction['transaction_type'] == 'U':
                        transaction['amount'] = '-'
                    else:
                        transaction['amount'] = data.find('amount').text
                    if transaction['transaction_type'] == 'R':
                        transaction['points_pbp'] = data.find('points_pbp').text
                    transaction['ip_cash_desk'] = data.find('ip_cash_desk').text
                    transaction['pos_version'] = data.find('pos_version').text
                    try:
                        transaction['bin'] = data.find('bin').text
                        continue
                    except:
                        transaction['totp'] = '-'
                transaction['products'] = []
                for item in root.iter('item'):
                    product = {}
                    product['groupCode'] = item.find('groupCode').text
                    product['code'] = item.find('code').text
                    product['quantity'] = item.find('quantity').text
                    product['amount'] = item.find('amount').text
                    transaction['products'].append(product)
                state = 'response'
        elif state == 'response':
            if '[PlSpasiboCardManagement] RESPONSE:' in line:
                response = line
                for l in lines[i + 1:]:
                    if '</ResponseData>' in response:
                        break 
                    response += l
                xml = response.split('RESPONSE: ')[-1]
                root = ET.fromstring(xml)
                transaction['success_response'] = 1
                for data in root.iter('ResponseData'):
                    transaction['response_receiptId'] = data.find('receiptId').text
                    date = data.find('processingDate').text
                    transaction['response_processingDate'] = date[6:8] + ':' + date[8:10] + ':' + date[10:12] + ' ' + \
                                                             date[4:6] + '-' + date[2:4] + '-' + date[0:2]
                    transaction['response_points'] = data.find('points').text
                    transaction['response_balance'] = data.find('balance').text
                    transaction['response_text'] = data.find('text').text
                    if transaction['response_text']:
                        transaction['response_text'] = transaction['response_text'].replace(';', '<br>')
                    transaction['response_errorCode'] = data.find('errorCode').text
                transactions.append(transaction)
                state = 'request'
            elif 'TRANSACTION ERROR:' in line:
                transaction['success_response'] = 0
                transaction['response_errorCode'] = line.split('TRANSACTION ERROR: ')[-1]
                transactions.append(transaction)
                state = 'request'
    return transactions


def get_ip(host, pos_id):
    if int(pos_id) < 10:
        pos_id = '0' + pos_id
    host = 'POS{}-BO-{}'.format(pos_id, host)
    z = ZabbixAPI('http://msk-dpro-app351/')
    z.login("store_card_api", "12345")
    ip = z.do_request('host.get', {
        'search': {'host': host},
        'selectInterfaces': ['ip'],
        'output': ['interfaces']
    })['result'][0]['interfaces'][0]['ip']
    z.session.close()
    return ip


def get_log_file(date):
    now = str(datetime.now().date())
    if date == now:
        log = 'loycard.log'
    else:
        date = date.replace('-', '')
        log = 'loycard.log-' + date + '0000.gz'
    return log


def parser(host, pos_id, date):
    for file in os.listdir(os.getcwd() + '/loyalty/logs'):
        os.remove(os.getcwd() + '/loyalty/logs/' + file)
    try:
        log = get_log_file(date)
        ip = get_ip(host, pos_id)
        path = get_log(ip, log)
        transactions = parse_log(path)
        os.remove(path)
        data = {'data': transactions}
        response = data
    except FileNotFoundError:
        try:
            os.remove(path)
        except:
            pass
        return {'error': 'Лог не найден'}
    except:
        try:
            os.remove(path)
        except:
            pass
        return {'error': 'Неизвестная ошибка. Опишите запрос на andrey.gavrilov@x5.ru.'}
    return response
