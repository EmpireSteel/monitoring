from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import Post
from .forms import LoginForm
from django.contrib.auth.decorators import login_required
from .ad import checkUserInAD
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.

@login_required
def index(request):
    all_posts = Post.objects.all()
    for post in all_posts:
        post.created = str(post.created)[:-6]
    return render(request, 'main/main_index.html', {'posts': all_posts})


def sign_in(request):
    if request.user.is_authenticated:
        return redirect(index)
    else:
        if request.method == 'POST':
            form = LoginForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data['username'].lower()
                password = form.cleaned_data['password']
                if username == 'admin':
                    user = authenticate(request, username=username, password=password)
                    if user is not None:
                        login(request, user)
                        return redirect(index)
                else:
                    if '@' in username:
                        username = username.split('@')[0]
                    if checkUserInAD(username+'@x5.ru', password):
                        user = authenticate(request, username=username, password=password)
                        if user is not None:
                            login(request, user)
                            if 'next' in request.GET:
                            	return HttpResponseRedirect(request.GET['next'])
                            return redirect(index)
                        else:
                            if User.objects.filter(username=username).exists():
                                user = User.objects.get(username__exact=username)
                                user.set_password(password)
                                user.save()
                                login(request, user)
                                return redirect(index)
                            else:
                                user = User.objects.create_user(username, username+'@x5.ru', password)
                                user.save()
                                login(request, user)
                                return redirect(index)
                    else:
                        return redirect(sign_in)
            else:
                return redirect(sign_in)
        else:
            form =LoginForm()
    return render(request, 'main/sign_in.html', {'form':form})

@login_required
def do_logout(request):
    logout(request)
    return redirect(sign_in)


