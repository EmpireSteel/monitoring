from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=250)
    author = models.ForeignKey(User, on_delete=models.CASCADE,
                                related_name='x5_news')
    body = models.TextField()
    created = models.DateTimeField(default=timezone.now)
    objects = models.Manager()
    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return self.title
