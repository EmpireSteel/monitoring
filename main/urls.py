from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='main_index'),
    path('sign_in/', views.sign_in, name='sign_in'),
    path('logout/', views.do_logout, name='logout'),
]