from django.urls import path
from . import views




urlpatterns = [
    path('', views.index, name='dashboard_index'),
    path('<str:server>/<str:page>/', views.pages, name='dashboard'),
    
]

