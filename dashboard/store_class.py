# coding=utf-8
from pyzabbix import ZabbixAPI
import os
import datetime
import psycopg2
from . import chart_plot


bo_rule = ('3', '4', '5', '6', '7', '8', '9', '4', 'L', 'Q', 'G', 'J', 'S', 'X', 'Y', 'H', 'E', 'O', 'V', 'D')
bf_rule1 = ('1', '2', 'B')
bf_rule2 = ('4000', '4001', '4002', '4003', '4004', '4005', '4006')


class Store:
    def __init__(self):
        self.sap = ''
        self.bo_zabbix = ZabbixAPI('http://msk-dpro-app351/')
        self.bo_zabbix.login("store_card_api", "12345")
        self.bf_zabbix = ZabbixAPI('http://zabbix-head.x5.ru/')
        self.bf_zabbix.login("store_card_api", "12345")
        self.host = False
        self.hostgroups = {}
        self.applications = {}
        self.store = {}
        self.colors = ["#000000","#FFFF00","#1CE6FF","#FF34FF","#FF4A46","#008941","#006FA6","#A30059","#FFDBE5",
        "#7A4900","#0000A6","#63FFAC","#B79762","#004D43","#8FB0FF","#997D87","#5A0007","#809693","#FEFFE6","#1B4400",
        "#4FC601","#3B5DFF","#4A3B53","#FF2F80","#61615A","#BA0900","#6B7900","#00C2A0","#FFAA92","#FF90C9","#B903AA",
        "#D16100","#DDEFFF","#000035","#7B4F4B","#A1C299","#300018","#0AA6D8","#013349","#00846F","#372101","#FFB500",
        "#C2FFED","#A079BF","#CC0744","#C0B9B2","#C2FF99","#001E09","#00489C","#6F0062","#0CBD66","#EEC3FF","#456D75",
        "#B77B68","#7A87A1","#788D66","#885578","#FAD09F","#FF8A9A","#D157A0","#BEC459","#456648","#0086ED","#886F4C",
        "#34362D","#B4A8BD","#00A6AA","#452C2C","#636375","#A3C8C9","#FF913F","#938A81","#575329","#00FECF","#B05B6F",
        "#8CD0FF","#3B9700","#04F757","#C8A1A1","#1E6E00","#7900D7","#A77500","#6367A9","#A05837","#6B002C","#772600",
        "#D790FF","#9B9700","#549E79","#FFF69F","#201625","#72418F","#BC23FF","#99ADC0","#3A2465","#922329","#5B4534",
        "#FDE8DC","#404E55","#0089A3","#CB7E98","#A4E804","#324E72","#6A3A4C","#83AB58","#001C1E","#D1F7CE","#004B28",
        "#C8D0F6","#A3A489","#806C66","#222800","#BF5650","#E83000","#66796D","#DA007C","#FF1A59","#8ADBB4","#1E0200",
        "#5B4E51","#C895C5","#320033","#FF6832","#66E1D3","#CFCDAC","#D0AC94","#7ED379","#012C58","#7A7BFF","#D68E01",
        "#353339","#78AFA1","#FEB2C6","#75797C","#837393","#943A4D","#B5F4FF","#D2DCD5","#9556BD","#6A714A","#001325",
        "#02525F","#0AA3F7","#E98176","#DBD5DD","#5EBCD1","#3D4F44","#7E6405","#02684E","#962B75","#8D8546","#9695C5",
        "#E773CE","#D86A78","#3E89BE","#CA834E","#518A87","#5B113C","#55813B","#E704C4","#00005F","#A97399","#4B8160",
        "#59738A","#FF5DA7","#F7C9BF","#643127","#513A01","#6B94AA","#51A058","#A45B02","#1D1702","#E20027","#E7AB63",
        "#4C6001","#9C6966","#64547B","#97979E","#006A66","#391406","#F4D749","#0045D2","#006C31","#DDB6D0","#7C6571",
        "#9FB2A4","#00D891","#15A08A","#BC65E9","#FFFFFE","#C6DC99","#203B3C","#671190","#6B3A64","#F5E1FF","#FFA0F2",
        "#CCAA35","#374527","#8BB400","#797868","#C6005A","#3B000A","#C86240","#29607C","#402334","#7D5A44","#CCB87C",
        "#B88183","#AA5199","#B5D6C3","#A38469","#9F94F0","#A74571","#B894A6","#71BB8C","#00B433","#789EC9","#6D80BA",
        "#953F00","#5EFF03","#E4FFFC","#1BE177","#BCB1E5","#76912F","#003109","#0060CD","#D20096","#895563","#29201D",
        "#5B3213","#A76F42","#89412E","#1A3A2A","#494B5A","#A88C85","#F4ABAA","#A3F3AB","#00C6C8","#EA8B66","#958A9F",
        "#BDC9D2","#9FA064","#BE4700","#658188","#83A485","#453C23","#47675D","#3A3F00","#061203","#DFFB71","#868E7E",
        "#98D058","#6C8F7D","#D7BFC2","#3C3E6E","#D83D66","#2F5D9B","#6C5E46","#D25B88","#5B656C","#00B57F","#545C46",
        "#866097","#365D25","#252F99","#00CCFF","#674E60","#FC009C","#92896B"]
        self.version = '2.5.1'

    def get_x5_stores(self):
        self.x5_stores = ''
        answer = self.bo_zabbix.do_request('template.get',
                          {'templateids': '130594','selectHosts': ['name'], 'output':['hosts']})['result'][0]['hosts']
        self.x5_stores = [i['name'] for i in answer]

    def get_bo_hostgroups(self):
        self.hostgroups['bo'] = self.bo_zabbix.do_request('hostgroup.get', {
            'search': {
                'name': 'RU/Филиал'
            },
            'output': ['groupid', 'name'],
            'sortfield': 'name'
        })['result']
        for i in self.hostgroups['bo']:
            i['name'] = i['name'][3:]
        for i in self.hostgroups['bo']:
            i['hosts'] = []
            i['hosts'].extend(self.bo_zabbix.do_request('host.get', {
                'groupids': i['groupid'],
                'search': {'host': 'BO*'},
                'searchWildcardsEnabled': True,
                'output': ['host'],
                'sortfield': 'host'
            })['result'])

 
    def get_init(self, page, server):
        self.host = server.upper()
        self.get_bo_items()
        self.get_bo_applications()
        self.get_bo_pos_applications()
        if page == 'dashboard':
            self.get_bo_pos_ping_status()
            self.get_bo_uptime()
            self.get_bo_problems()
            self.store['bo']['graphics'] = chart_plot.get_bo_data(self.store, self.bo_zabbix)
            self.store['poses']['graphics'] = chart_plot.get_pos_data(self.store['poses'], self.bo_zabbix)
        elif page == 'server':
            self.get_bo_bacchus()
            self.get_bo_processor()
            self.get_bo_utm_monitor()
            self.get_bo_system()
            self.get_bo_memory()
            self.get_bo_os()
            self.get_bo_product()
            self.get_bo_ram()
            self.get_close_day()
        elif page == 'poses':
            self.get_bo_pos_bacchus()
            self.get_bo_pos_processor()
            self.get_bo_pos_fz54()
            self.get_bo_pos_os()
            self.get_bo_pos_ram()
            self.get_bo_pos_memory()
            self.get_bo_pos_system()
            self.get_bo_pos_product()
            self.get_last_check()
            self.get_last_alk()
            self.get_z()
        elif page == 'network':
            self.get_bo_network()
        elif page == 'promo':
            self.get_promo()
            self.get_null_prices()


    
    # ------------------------------------------Общее-------------------------------------------------

    def detect(self, name):
        self.sap = name
        if name.upper().startswith("BO"):
            self.type = 'bo'
        elif name.upper().startswith("SUPER") or name.upper().startswith("GIPER"):
            self.type = 'bf'
            name = self.get_bf_full_name(name)
        else:
            if name.upper().startswith(bf_rule1) or name.upper() in bf_rule2:
                self.type = 'bf'
                name = self.get_bf_full_name(name)
            elif name.upper().startswith(bo_rule):
                name = 'BO-' + name
                self.type = 'bo'
        self.host = name
        return name


    def get_ping_status(self, item):
        try:
            for i in item:
                i['status'] = 'online' if os.system("fping -t 1 " + i['ip']) == 0 else 'offline'
            return item
        except:
            item['status'] = 'online' if os.system("fping -t 1 " + item['ip']) == 0 else 'offline'
            return item



    # ------------------------------------------BO-------------------------------------------------

    def get_bo_applications(self):
        self.applications['bo'] = {}
        applications = self.bo_zabbix.do_request('application.get', {
            "hostids": self.store['bo']['hostid'],
            'output': ['name']
        })['result']
        for i in applications:
            self.applications['bo'][i['name']] = i['applicationid']

    def get_bo_pos_applications(self):
        self.applications['pos'] = []
        for i in self.store['poses']['hostid']:
            apps = {}
            applications = self.bo_zabbix.do_request('application.get', {
                "hostids": i,
                'output': ['name']
            })['result']
            for i in applications:
                apps[i['name']] = i['applicationid']
            self.applications['pos'].append(apps)

    def get_bo_items(self):
        self.store['bo'] = {}
        self.store['cash'] = {}
        self.store['tsd'] = []
        self.store['bio'] = {}
        find = self.bo_zabbix.do_request('host.get', {
            'search': {
                'name': self.host[3:]
            },
            'selectInterfaces': ['ip'],
            'output': 'extend'
        })['result']
        elements = [{'name': i['name'], 'hostid': i['hostid'], 'ip': i['interfaces'][0]['ip']} for i in find]
        elements = sorted(elements, key=lambda k: k['ip'])
        self.store['poses'] = {'name': [], 'hostid': [], 'ip': [], 'metrics': {}}
        for i in elements:
            if i['name'].startswith("BO-"):
                self.store['bo'] = i
            elif i['name'].startswith("CC"):
                self.store['cash'] = i
            elif "SAP-" + self.host[3:] in i['name']:
                self.store['bio'] = i
            elif i['name'].startswith("POS"):
                self.store['poses']['name'].append(i['name'])
                self.store['poses']['hostid'].append(i['hostid'])
                self.store['poses']['ip'].append(i['ip'])
        tsd = self.bo_zabbix.do_request('item.get', {'hostids': self.store['bo']['hostid'], 'search': {'key_': 'tsd.ip'}, 'output': ['name', 'lastvalue']})['result']
        self.store['tsd'] = [{'name': i['name'][7:], 'ip': i['lastvalue']} for i in tsd if str(i['lastvalue']) != '0']
        self.store['bo'] = self.get_ping_status(self.store['bo'])
        try:
            self.store['cash'] = self.get_ping_status(self.store['cash'])
        except:
            self.store['cash'] = []
        try:
            self.store['bio'] = self.get_ping_status(self.store['bio'])
        except:
            self.store['bio'] = []
        self.store['tsd'] = self.get_ping_status(self.store['tsd'])
        self.store['tsd'] = [i for i in self.store['tsd'] if i['status']=='online']
        # self.store['tsd'] = sorted(self.store['tsd'], key=lambda k: k['status'])


    def get_bo_pos_ping_status(self):
        self.store['poses']['status'] = []
        for i in self.store['poses']['ip']:
            self.store['poses']['status'].append('online' if os.system("fping -t 1 " + i) == 0 else 'offline')

    def get_bo_uptime(self):
        self.store['bo']['uptime'] = self.bo_zabbix.do_request('item.get', {
            'hostids': self.store['bo']['hostid'],
            'search': {
                'key_': 'system.uptime'
            },
            'output': ['name', 'lastvalue']})["result"][0]['lastvalue']
        self.store['bo']['uptime'] = datetime.timedelta(seconds=int(self.store['bo']['uptime']))
        find = []
        for i in self.store['poses']['hostid']:
            find.extend(self.bo_zabbix.do_request('item.get', {
                'hostids': i,
                'search': {
                    'key_': 'system.uptime'
                },
                'output': ['name', 'lastvalue']})["result"])
        self.store['poses']['uptime'] = [datetime.timedelta(seconds=int(i['lastvalue'])) for i in find]

    def get_bo_bacchus(self):
        try:
            self.store['bo']['bacchus'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.applications['bo']['Bacchus'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
        except:
            self.store['bo']['bacchus'] = []

    def get_bo_processor(self):
        
        self.store['bo']['processor'] = self.bo_zabbix.do_request("application.get", {
            "applicationids": self.applications['bo']['Processor Information'],
            'selectItems': ['name', 'lastvalue']
        })['result'][0]['items']
        for i in range(len(self.store['bo']['processor'])):
            if self.store['bo']['processor'][i]['name'] == 'Утилизация CPU':
                self.store['bo']['processor'][i]['lastvalue'] = str(round(float(self.store['bo']['processor'][i]['lastvalue']), 2)) + " %"
    

    def get_bo_utm_monitor(self):
        try:
            self.store['bo']['utm_monitor'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.applications['bo']['UTM-MONITOR'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
        except:
            self.store['bo']['utm_monitor'] = []

    def get_bo_system(self):
        try:
            self.store['bo']['system'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.applications['bo']['System'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            for i in range(len(self.store['bo']['system'])):
                if self.store['bo']['system'][i]['name'] == 'Аптайм':
                    self.store['bo']['system'][i]['lastvalue'] = datetime.timedelta(seconds=int(self.store['bo']['system'][i]['lastvalue']))
                if self.store['bo']['system'][i]['name'] == 'Местное время':
                    self.store['bo']['system'][i]['lastvalue'] = datetime.datetime.fromtimestamp(int(self.store['bo']['system'][i]['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S')
        except:
            self.store['bo']['system'] = []

    def get_bo_memory(self):
        try:
            self.store['bo']['memory'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.applications['bo']['Memory'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            for i in self.store['bo']['memory']:
                if i['name'] == "Общее количество дискового пространства в /" or \
                        i['name'] == "Свободно дискового пространства в / (GB)":
                    i['lastvalue'] = str(round(float(i['lastvalue']) / 1024 / 1024 / 1024, 2)) + " Gb"
                if i['name'] == 'Свободно дискового пространства в / (в процентах)':
                    i['lastvalue'] = str(round(float(i['lastvalue']), 2)) + " %"
        except:
            self.store['bo']['memory'] = []

    def get_bo_os(self):
        try:
            self.store['bo']['os'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.applications['bo']['OS Information'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
        except:
            self.store['bo']['os'] = []

    def get_bo_product(self):
        try:
            self.store['bo']['product'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.applications['bo']['Product Information'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
        except:
            self.store['bo']['product'] = []

    def get_bo_ram(self):
        try:
            self.store['bo']['ram'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.applications['bo']['RAM'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
            for i in self.store['bo']['ram']:
                if i['name'] == 'Количество используемой памяти' \
                        or i['name'] == 'Общее количество памяти' or i['name'] == 'Свободно памяти':
                    i['lastvalue'] = str(round(float(i['lastvalue']) / 1024 / 1024 / 1024, 2)) + " Gb"
                else:
                    i['lastvalue'] = str(round(float(i['lastvalue']), 2)) + " %"
        except:
            self.store['bo']['ram'] = []

    def get_bo_network(self):
        try:
            self.store['bo']['network'] = self.bo_zabbix.do_request("application.get", {
                "applicationids": self.applications['bo']['Router'],
                'selectItems': ['name', 'lastvalue']
            })['result'][0]['items']
        except:
            self.store['bo']['network'] = []

    def get_close_day(self):
        conn_string = "host=" + str(self.store['bo']['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
        SELECT ra."create_timestamp"
        FROM gk_report_archive ra
        WHERE ra."type_code" = 'XRG_TILL_FINANCIAL'
        ORDER BY ra."create_timestamp" DESC
        LIMIT 1""")
        records = cursor.fetchall()
        conn.close()
        self.store['bo']['close_day'] = [{'date': i[0]} for i in records]

    def get_bo_pos_bacchus(self):
        try:
            items = {}
            for i in self.applications['pos']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['Bacchus'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
               
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    if j['name'] == 'Касса использует Bacchus(gk_functions.xml)':
                        if j['lastvalue'] == 'GK':
                            items[j['name']].append('true')
                        elif j['lastvalue'] == 'SAP_PI':
                            items[j['name']].append('false')
                        else:
                            items[j['name']].append('не поддерживается')
                    else:
                        if j['lastvalue'] == '0':
                            items[j['name']].append('не поддерживается')
                        else:
                            items[j['name']].append(j['lastvalue'])
            
            self.store['poses']['metrics']['bacchus'] = items
        except:
            self.store['poses']['metrics']['bacchus'] = {
                '': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}


    def get_bo_pos_processor(self):
        try:
            items = {}
            for i in self.applications['pos']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['Processor Information'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']


                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []

                    if j['name'] == 'Утилизация CPU':
                        items[j['name']].append(str(round(float(j['lastvalue']), 2)) + " %")
                    else:
                        if len(j['lastvalue']) >= 18:
                            items[j['name']].append(j['lastvalue'][:18] + "\n" + j['lastvalue'][18:])
                        else:
                            items[j['name']].append(j['lastvalue'])
            self.store['poses']['metrics']['Процессор'] = items
        except:
            self.store['poses']['metrics']['Процессор'] = {'': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}


    def get_bo_pos_fz54(self):
        try:
            items = {}
            for i in self.applications['pos']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['FZ54'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    if j['name'] == 'Дата активизации ФН':
                        items[j['name']].append(datetime.datetime.fromtimestamp(int(j['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S'))
                    elif j['name'] == 'Дата последнего документа отправленного в ОФД':
                        if int(j['lastvalue']) != 0:
                            items[j['name']].append(datetime.datetime.fromtimestamp(int(j['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S'))
                        else:
                            items[j['name']].append('никогда')
                    elif j['name'] == 'Дата создания файла info_FR_human':
                        if int(j['lastvalue']) != 0:
                            items[j['name']].append(datetime.datetime.fromtimestamp(int(j['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S'))
                        else:
                            items[j['name']].append('никогда')
                    else:
                        items[j['name']].append(j['lastvalue'])
            self.store['poses']['metrics']['ФЗ54'] = items
        except :
            self.store['poses']['metrics']['ФЗ54'] = {'': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_pos_init(self):
        try:
            items = {}
            for i in self.applications['pos']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['Initialization'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    items[j['name']].append(j['lastvalue'])
            self.store['poses']['metrics']['Инициализация устройств'] = items
        except:
            self.store['poses']['metrics']['Инициализация устройств'] = {'': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_pos_os(self):
        try:
            items = {}
            for i in self.applications['pos']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['OS Information'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    items[j['name']].append(j['lastvalue'])
            self.store['poses']['metrics']['Операционная система'] = items
        except:
            self.store['poses']['metrics']['Операционная система'] = {'': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_pos_ram(self):
        try:
            items = {}
            for i in self.applications['pos']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['RAM'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    if "в процентах" in j['name']:
                        items[j['name']].append(str(round(float(j['lastvalue']), 2)) + " %")
                    else:
                        items[j['name']].append(str(round(float(j['lastvalue']) / 1024 / 1024, 2)) + " Mb")
            self.store['poses']['metrics']['Оперативная память'] = items
        except:
            self.store['poses']['metrics']['Оперативная память'] = {
            '': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_pos_memory(self):
        try:
            items = {}
            for i in self.applications['pos']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['Memory'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    if "в процентах" in j['name']:
                        items[j['name']].append(str(round(float(j['lastvalue']), 2)) + " %")
                    elif "RAID" in j['name']:
                        items[j['name']].append(j['lastvalue'])
                    else:
                        items[j['name']].append(str(round(float(j['lastvalue']) / 1024 / 1024 / 1024, 2)) + " Gb")
            self.store['poses']['metrics']['Память'] = items
        except:
            self.store['poses']['metrics']['Память'] = {
            '': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_pos_system(self):
        try:
            items = {}
            for i in self.applications['pos']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['System'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    if j['name'] == 'Аптайм':
                        items[j['name']].append(datetime.timedelta(seconds=int(j['lastvalue'])))
                    elif j['name'] == 'Местное время':
                        items[j['name']].append(datetime.datetime.fromtimestamp(int(j['lastvalue'])).strftime('%d-%m-%Y %H:%M:%S'))
                    else:
                        items[j['name']].append(j['lastvalue'])
            self.store['poses']['metrics']['Система'] = items
        except:
            self.store['poses']['metrics']['Система'] = {
                '': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_pos_product(self):
        try:
            items = {}
            for i in self.applications['pos']:
                result = self.bo_zabbix.do_request("application.get", {
                    "applicationids": i['Product Information'],
                    'selectItems': ['name', 'lastvalue']
                })['result'][0]['items']
                result = sorted(result, key=lambda k: k['name'])
                for j in result:
                    if j['name'] not in items.keys():
                        items[j['name']] = []
                    items[j['name']].append(j['lastvalue'])
            self.store['poses']['metrics']['Устройство'] = items
        except:
            self.store['poses']['metrics']['Устройство'] = {
                '': ['не поддерживается' for i in range(len(self.store['poses']['name']))]}

    def get_bo_problems(self):
        self.store['problems'] = {}
        for i in [self.store['bo'], self.store['cash'], self.store['bio']]:
            if i:
                find = self.bo_zabbix.do_request("trigger.get", {
                    "host": i['name'],
                    "expandDescription": "1",
                    "output": ['description', 'lastchange'],
                    "filter": {'value': 1},
                })['result']
                if find:
                    self.store['problems'][i['name']] = []
                    for j in find:
                        self.store['problems'][i['name']].append({'name': j['description'], 'date': datetime.datetime.fromtimestamp(int(j['lastchange']))
                                                .strftime('%Y-%m-%d %H:%M:%S')}) 
        for i in self.store['poses']['name']:
            pos_find = self.bo_zabbix.do_request("trigger.get", {
                "host": i,
                "expandDescription": "1",
                "output": ['description', 'lastchange'],
                "filter": {'value': 1},
            })['result']
            if pos_find:
                self.store['problems'][i] = []
                for j in pos_find:
                    self.store['problems'][i].append({'name': j['description'],
                                        'date': datetime.datetime.fromtimestamp(int(j['lastchange']))
                                            .strftime('%Y-%m-%d %H:%M:%S')})
        self.dashboard_server = 'success'
        self.dashboard_pos = 'success'
        for i in self.store['poses']['name']:
            if i in self.store['problems'].keys():
                self.dashboard_pos = 'danger'
        for i in [self.store['bo'], self.store['cash'], self.store['bio']]:
            if i:
                if i['name'] in self.store['problems'].keys():
                    self.dashboard_server = 'danger'

    def get_last_check(self):
        conn_string = "host=" + str(self.store['bo']['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
        SELECT max(bk.aktdat), bk."workstation_id"
        FROM "gkretail"."gk_bonkopf" bk
        GROUP BY bk."workstation_id"
        ORDER BY bk."workstation_id"
""")
        records = cursor.fetchall()
        records = records[1:]
        conn.close()
        self.store['poses']['last_check'] = [{'pos': i, 'date': 'нет данных'} for i in self.store['poses']['name']]
        for i in self.store['poses']['last_check']:
            for j in records:
                if int(i['pos'][3:5]) == j[1]:
                    i['date'] = j[0]

    def get_last_alk(self):
        conn_string = "host=" + str(self.store['bo']['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
        SELECT DISTINCT on (y.workstation_id) max(y."aktdat"), y.workstation_id, nn.nm_itm
        FROM gkretail.gk_bonkopf y, gkretail.gk_bonposition z, gkretail.as_itm nn
        WHERE
        z.artnr IN (SELECT item_id FROM xrg_item WHERE alcohol_flag = 'J' and TAX_INPUT_REQUIRED_FLAG='J')
        AND z.bon_seq_id = y.bon_seq_id
        AND nn.id_itm = z.artnr
        GROUP BY y.workstation_id, y.aktdat, nn.nm_itm
        ORDER BY y.workstation_id ASC, y.aktdat DESC""")
        records = cursor.fetchall()
        conn.close()
        self.store['poses']['last_alk'] = [{'pos': i, 'date': 'нет данных', 'item': 'нет данных'} for i in self.store['poses']['name']]
        for i in self.store['poses']['last_alk']:
            for j in records:
                if int(i['pos'][3:5]) == j[1]:
                    i['date'] = j[0]
                    i['item'] = j[2]

    def get_z(self):
        conn_string = "host=" + str(self.store['bo']['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
        SELECT DISTINCT on (b."lade_nr") max(b."aktdat"), b."lade_nr"
        FROM "gkretail"."gk_bonkopf" b
        WHERE b."workstation_id" = 0
        AND b."belegtyp" = 501
        GROUP BY b."lade_nr", "aktdat"
        ORDER BY b."lade_nr" ASC, b."aktdat" DESC""")
        records = cursor.fetchall()
        conn.close()
        self.store['poses']['z'] = [{'pos': i, 'date': 'нет данных'} for i in self.store['poses']['name']]
        for i in self.store['poses']['z']:
            for j in records:
                if int(i['pos'][3:5]) == j[1]:
                    i['date'] = j[0]

    def get_null_prices(self):
        conn_string = "host=" + str(self.store['bo']['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
        select ids.item_id, names.de_itm, prices.price_amount
        from xrg_item as ids 
        join as_itm as names on ids.item_id=names.id_itm
        join gk_item_selling_prices as prices on ids.item_id=prices.item_id
        where 
        prices.price_amount = 0
        order by ids.item_id""")
        records = cursor.fetchall()
        conn.close()
        self.store['null_prices'] = []
        for i in records:
            self.store['null_prices'].append({'ids': i[0], 'item': i[1]})

    def get_promo(self):
        conn_string = "host=" + str(self.store['bo']['ip']) + " dbname='postgres' user='gkretail' password='gkretail'"
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()
        cursor.execute("""
            SELECT "nm_prm_prt", "description"
            FROM "gkretail"."co_prm"
            ORDER BY "nm_prm_prt" DESC """)
        records = cursor.fetchall()
        conn.close()
        self.store['promo'] = []
        for i in records:
            self.store['promo'].append({'name': i[0], 'description': i[1]})