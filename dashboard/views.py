from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from.store_class import Store
from django.http import HttpResponseRedirect
from django.urls import reverse


store = Store()



# Create your views here.
@login_required
def pages(request, server, page):
    if server != 'False':
        if request.method == 'POST':
            search = request.POST.get('store').upper()
            search = store.detect(search)
            return HttpResponseRedirect(reverse('dashboard', args=(search, page)))
        else:
            store.get_init(page, server)

            return render(request, 'dashboard/{}.html'.format(page), {'store':store})
    else:
        return render(request, 'dashboard/dashboard_index.html', {'store':store})

@login_required
def index(request):
    store.get_x5_stores()
    if request.method == 'POST':
        search = request.POST.get('store').upper()
        search = store.detect(search)
        return HttpResponseRedirect(reverse('dashboard', args=(search, 'dashboard')))
    else:
        return render(request, 'dashboard/dashboard_index.html', {'store':store})

