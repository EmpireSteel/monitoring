import xlrd
import csv
import os


def csv_from_excel():
    wb = xlrd.open_workbook(os.path.join(os.getcwd(), 'support', 'files', 'file.xlsx'))
    sh = wb.sheet_by_name('Лист1')
    your_csv_file = open(os.path.join(os.getcwd(), 'support', 'files', 'file.csv'), 'w')
    wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)

    for rownum in range(sh.nrows):
        wr.writerow(sh.row_values(rownum))

    your_csv_file.close()


# def csv_parser():
#     users = {}
#     with open(os.path.join(os.getcwd(), 'support', 'files', 'file.csv'), encoding='cp1251') as f:
#         lines = f.readlines()
#     header = lines.pop(0).strip().replace('"', '').split(',')
#     lines = [line.strip().replace('"', '').split(',') for line in lines if line.strip().replace('"', '').split(',') != ['']]
#     for line in lines:
#         if not line[5] in users:
#             users[line[5]] = {}
# runs the csv_from_excel function:
csv_from_excel()
# csv_parser()