from django.contrib import admin
from django.urls import path, include
from . import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('dashboard/', include('dashboard.urls')),
    path('loyalty/', include('loyalty.urls')),
    path('bms/', include('bms.urls')),
    path('support/', include('support.urls')),
    path('', include('main.urls'))
]


if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    urlpatterns += staticfiles_urlpatterns()