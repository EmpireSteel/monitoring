$(document).ready(function () {
    max_error =  parseInt(max_error);
    var ctx = document.getElementById('myChart').getContext('2d');

    var speedData = {
        labels: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", 'Октябрь', 'Ноябрь',
            "Декабрь"
        ],
        datasets: [{
                label: "Проданная алкогольная продукция",
                borderColor: 'rgb(54, 162, 235)',
                backgroundColor: 'rgb(54, 162, 235)',
                fill: false,
                data: [9500946499, 9031966528, 10384437020, 9423622941, 10165884348, 10054867403, 10160753999, 10575400957, 10078052972, 10978840971, 11432647263, 16449581933],
                yAxisID: 'y-axis-1',
            },
            {
                label: "Ошибки",
                borderColor: 'rgb(255, 99, 132)',
                backgroundColor: 'rgb(255, 99, 132)',
                fill: false,
                data: [max_error*1.1, max_error*1.05, max_error*0.95, max_error*1.05, max_error*0.9, max_error*0.93, max_error, max_error, max_error, max_error, max_error, max_error],
                yAxisID: 'y-axis-2',
            }
        ]
    };

    var chartOptions = {
        legend: {
            display: true,
            position: 'top',
            labels: {
                boxWidth: 80,
                fontColor: 'black'
            }
        },
        scales: {
            yAxes: [{
                display: true,
                position: 'left',
                id: 'y-axis-1',
                ticks: {
                    callback: function (value) {
                        return "₽" + numeral(value).format('0,0')
                    }
                    
                }
            },{
                display: true,
                position: 'right',
                id: 'y-axis-2', 
                ticks: {
                    suggestedMin: 0,
                    callback: function (value) {
                        return numeral(value).format('0,0')
                    }
                }
            }]
        },
        title: {
            display: false
        },
        tooltips: {
            callbacks: {
                label: function (tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    if (label.includes('Ошибки')) {
                        label += numeral(tooltipItem.yLabel).format('0,0');
                    }
                    else {
                        label += "₽ " + numeral(tooltipItem.yLabel).format('0,0');
                    }
                    
                    return label;
                }
            }
        }
    };
    var myChart = new Chart(ctx, {
        type: 'line',
        data: speedData,
        options: chartOptions
    });





    $("#money").change(function () {
        $("#errors").val(100 - $("#money").val());
        var values = check_errors();
        var rto = values[0];
        var c_errors = values[1];
        var fte = values[2];
        $("#res_sum").text("₽ " + String(rto).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        $("#fte").text(fte);
        var m_per_month = rto / 6;
        var e_per_month = c_errors / 6;
        update_graph(m_per_month, e_per_month, myChart);

    })

    $("#errors").change(function () {
        $("#money").val(100 - $("#errors").val());
        var values = check_errors();
        var rto = values[0];
        var c_errors = values[1];
        var fte = values[2];
        $("#res_sum").text("₽ " + String(rto).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        $("#fte").text(fte);
        var m_per_month = rto / 6;
        var e_per_month = c_errors / 6;
        update_graph(m_per_month, e_per_month, myChart);

    })

    $(".error_check").change(function () {
        var errors = [];
        $("input[id^=check_]").each(function (item) {
            errors.unshift(item)
        });
        var current_money = 0;
        var current_errors = 0;
        var fte = 0;
        $.each(errors, function (index, item) {
            if ($("#check_" + item).is(':checked')) {
                current_money += parseFloat($("#money_" + item).text().replace(/ /g, ''));
                current_errors += parseFloat($("#error_" + item).text().replace(/ /g, ''));
                fte += parseFloat($("#fte_" + item).text().replace(/ /g, ''));
            }
            
        })
        var range_money = current_money / max_money * 100;
        $("#money").val(range_money);
        $("#errors").val(100 - range_money);
        
        var m_per_month = range_money / 6;
        var e_per_month = current_errors / 6;
        update_graph(m_per_month, e_per_month, myChart);
        
        $("#res_sum").text("₽ " + String(current_money).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        $("#fte").text(fte);
    })
})

function update_graph(m_per_month, e_per_month, chart){
    var alco = [9500946499, 9031966528, 10384437020, 9423622941, 10165884348, 10054867403, 10160753999 + m_per_month, 10575400957 + 2 * m_per_month, 10078052972 +  3 * m_per_month, 10978840971 + 4 * m_per_month, 11432647263 + 5 * m_per_month, 16449581933 + 6 * m_per_month]
    var er = [max_error*1.1, max_error*1.05, max_error*0.95, max_error*1.05, max_error*0.9, max_error*0.93, max_error - e_per_month, max_error - 2 * e_per_month, max_error - 3 * e_per_month, max_error - 4 * e_per_month, max_error - 5 * e_per_month, max_error - 6 * e_per_month];
    chart.data.datasets[0].data = alco;
    chart.data.datasets[1].data = er;
    chart.update();
}

function check_errors(){
    var errors = [];
    $("input[id^=check_]").each(function (item) {
        errors.unshift(item)
    });
    var rto = Math.floor($("#money").val() * max_money / 100);
    var c_errors = max_error - (100 - $("#money").val()) * max_error / 100;
    var tmp_money = 0;
    var tmp_fte = 0;
    $.each(errors, function (index, item) {
        tmp_money += parseFloat($("#money_" + item).text().replace(/ /g, ''))
        if (rto >= tmp_money) {
            $("#check_" + item).prop("checked", true);
            tmp_fte += parseFloat($("#fte_" + item).text().replace(/ /g, ''))   
        } else {
            $("#check_" + item).prop("checked", false);
        }
    })
    return [rto, c_errors, tmp_fte]
}