import random
from clickhouse_driver import Client
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

# Create your views here.
from django.utils.decorators import method_decorator
from django.views import generic, View
from django.views.decorators.csrf import csrf_exempt

d = {'АМ заблокирована. Отложите товар': 'PBI000000023185\nPBI000000023516\nPBI000000023184\nPBI000000023211',
'Попытка продажи дубля продукции': 'PBI000000023121',
'АМ уже продана. Отложите товар': 'PBI000000023611\nPBI000000023324',
'PLU/штрих-код не совпадает с отсканированной АМ': 'PBI000000023520\nPBI000000023519\nPBI000000023515\nPBI000000023514\nPBI000000023210',
'АМ запрещена к продаже. Отложите товар': 'PBI000000023326',
'Продукция не внесена в поштучный учет': 'PBI000000023122',
'Акцизная марка не продавалась в этом магазине': 'PBI000000023613',
'Продукция не поступала в магазин по поштучному учету': 'PBI000000022025\nPBI000000023090',
'АМ заблокирована на кассе. Отложите товар': 'PBI000000023513\nPBI000000023512\nPBI000000023612\nPBI000000023327'}

class IndexView(View):
    def get(self, request):
        # <view logic>
        c = Client('192.168.228.170')
        res = c.execute('select ERROR, COUNTS, LOSS from flink_bms.ALCO_LOSSES WHERE CHECK_DATE = (SELECT max(CHECK_DATE) from flink_bms.ALCO_LOSSES)')
        response = [i for i in res if i[2] > 0]
        poses = c.execute('select uniq(STORE_ID, POS_ID) from flink_bms.ALCO_TRANSACT where toDate(EVENT_DATE) = yesterday()')
        poses_c = int(50000*365/poses[0][0])
        data = []
        for i, v in enumerate(response):
            data.append([response[i][0], response[i][1] *  poses_c, round(response[i][2] * poses_c)])
        data.sort(key=lambda x: x[2], reverse=True)
        max_money = sum([i[2] for i in data])
        max_error = sum([i[1] for i in data])
        for i, v in enumerate(data):
            if v[0] in d:
                data[i].insert(0, d[v[0]]) 
            else:
               data[i].insert(0, '')
            data[i].insert(3, round(9/max_error*v[3], 4)) 
        money_c = 100/max_money
        error_c = 100/max_error
        str_data = []
        for i, v in enumerate(data):
            str_data.append([data[i][0], data[i][1],'{0:,}'.format(data[i][2]).replace(',', ' '), '{0:,}'.format(data[i][3]).replace(',', ' '), '{0:,}'.format(data[i][4]).replace(',', ' ')])
        return render(request, 'bms/index.html', context={'data': data, 'money_c': money_c, 'error_c': error_c, 'max_money': max_money, 'max_error':max_error, 'str_data': str_data})



